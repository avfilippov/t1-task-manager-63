package ru.avfilippov.tm.repository;

import ru.avfilippov.tm.model.Project;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public final class ProjectRepository {

    private static final ProjectRepository INSTANCE = new ProjectRepository();

    public static ProjectRepository getInstance() {
        return INSTANCE;
    }

    private Map<String, Project> projects = new LinkedHashMap<>();


    {
        add(new Project("TEST1"));
        add(new Project("TEST2"));
        add(new Project("TEST3"));
        add(new Project("TEST4"));

    }

    public void create() {
        add(new Project("New Project " + System.currentTimeMillis()));
    }

    public void add(final Project project) {
        projects.put(project.getId(), project);
    }

    public void save(final Project project) {
        projects.put(project.getId(), project);
    }

    public Collection<Project> findAll() {
        return projects.values();
    }

    public Project findById(final String id) {
        return projects.get(id);
    }

    public void removeById(final String id) {
        projects.remove(id);
    }

}
