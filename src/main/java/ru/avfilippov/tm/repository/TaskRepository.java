package ru.avfilippov.tm.repository;

import ru.avfilippov.tm.model.Project;
import ru.avfilippov.tm.model.Task;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public final class TaskRepository {

    private static final TaskRepository INSTANCE = new TaskRepository();

    public static TaskRepository getInstance() {
        return INSTANCE;
    }

    private Map<String, Task> tasks = new LinkedHashMap<>();


    {
        add(new Task("TEST1"));
        add(new Task("TEST2"));
        add(new Task("TEST3"));
        add(new Task("TEST4"));

    }

    public void create() {
        add(new Task("New Task " + System.currentTimeMillis()));
    }

    public void add(final Task task) {
        tasks.put(task.getId(), task);
    }

    public void save(final Task task) {
        tasks.put(task.getId(), task);
    }

    public Collection<Task> findAll() {
        return tasks.values();
    }

    public Task findById(final String id) {
        return tasks.get(id);
    }

    public void removeById(final String id) {
        tasks.remove(id);
    }

}
