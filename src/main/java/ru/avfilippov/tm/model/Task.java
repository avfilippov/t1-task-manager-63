package ru.avfilippov.tm.model;

import lombok.Getter;
import lombok.Setter;
import ru.avfilippov.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

public final class Task {

    @Setter
    @Getter
    private String id = UUID.randomUUID().toString();

    @Setter
    @Getter
    private String name;

    @Setter
    @Getter
    private String description;

    @Setter
    @Getter
    private Status status = Status.NOT_STARTED;

    @Setter
    @Getter
    private Date dateStart;

    @Setter
    @Getter
    private Date dateFinish;

    @Setter
    @Getter
    private String projectId;

    public Task() {
    }

    public Task(final String name) {
        this.name = name;
    }

}
