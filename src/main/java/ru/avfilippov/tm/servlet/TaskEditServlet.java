package ru.avfilippov.tm.servlet;

import lombok.SneakyThrows;
import ru.avfilippov.tm.enumerated.Status;
import ru.avfilippov.tm.model.Project;
import ru.avfilippov.tm.model.Task;
import ru.avfilippov.tm.repository.ProjectRepository;
import ru.avfilippov.tm.repository.TaskRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;

@WebServlet("/task/edit/*")
public class TaskEditServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String id = req.getParameter("id");
        final Task task = TaskRepository.getInstance().findById(id);
        req.setAttribute("task", task);
        req.setAttribute("statuses", Status.values());
        req.setAttribute("projects", ProjectRepository.getInstance().findAll());
        req.getRequestDispatcher("/WEB-INF/views/task-edit.jsp").forward(req,resp);
    }

    @SneakyThrows
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String id = req.getParameter("id");
        final String projectId = req.getParameter("projectId");
        final String name = req.getParameter("name");
        final String description = req.getParameter("description");
        final String statusValue = req.getParameter("status");
        final Status status = Status.valueOf(statusValue);
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        final String dateStartValue = req.getParameter("dateStart");
        final String dateFinishValue = req.getParameter("dateFinish");

        final Task task = new Task();
        task.setId(id);
        task.setProjectId(projectId);
        task.setName(name);
        task.setDescription(description);
        task.setStatus(status);


        if(!dateStartValue.isEmpty()) task.setDateStart(simpleDateFormat.parse(dateStartValue));
        else task.setDateStart(null);
        if(!dateFinishValue.isEmpty()) task.setDateFinish(simpleDateFormat.parse(dateFinishValue));
        else task.setDateFinish(null);

        TaskRepository.getInstance().save(task);
        resp.sendRedirect("/tasks");
    }

}
